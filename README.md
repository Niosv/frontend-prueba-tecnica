### Frontend de Prueba técnica

Esta aplicación fue desarrollada utilizando React e implementando Redux-Toolkit en conjunto de Redux-saga para realizar las peticiones a la API.

Las funciones de esta aplicación son:

- Listar artículos provenientes de la API (GET)
- Visualizar artículos específicos
- Login y registro de periodistas con encriptación de contraseña
- Cierre de sesión
- Crear artículos (POST)
- Modificar artículos que haya creado el periodista (PUT)
- Eliminar artículos que haya creado el periodista (DELETE)

## Comienzo

Para explorar la aplicación se debe crear una cuenta o utilizar las siguientes credenciales:

- **correo** : ejemplo@gmail.com
- **contraseña** : 123456

# Considerar

Para el correcto funcionamiento de la aplicación es necesario que la API [(En este repositorio)](https://gitlab.com/Niosv/api-prueba-tecnica) este levantada en el localhost en el puerto **3000**.
