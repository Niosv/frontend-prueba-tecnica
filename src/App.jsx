import './App.css'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import Index from './views/Index'
import Articulo from './views/Articulo'
import AddArticulo from './views/AddArticulo'
import ModArticulo from './views/ModArticulo'
import Login from './views/Login'
import Register from './views/Register'
import ProtectedRoute from './components/ProtectedRoute'
import Navbar from './components/Navbar'
import Footer from './components/Footer'

function App() {
  const role = 2 //perfil

  return (
    <>
      <Router>
        <main className='relative min-h-screen pb-16 bg-sky-50 '>
          <Navbar />
          <Routes>
            <Route path='/' Component={Index} />
            <Route path='/articulo/:id' Component={Articulo} />
            <Route path='/login' Component={Login} />
            <Route path='/register' Component={Register} />

            {/* Protected */}
            <Route element={<ProtectedRoute isAllowed={role === 2} />}>
              <Route path='/addArticulo' Component={AddArticulo} />
              <Route path='/modArticulo/:id' Component={ModArticulo} />
            </Route>
          </Routes>
          <Footer />
        </main>
      </Router>
    </>
  )
}

export default App
