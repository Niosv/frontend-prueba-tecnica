import React from 'react'
import { Link } from 'react-router-dom'

export default function Card({ articulo }) {
    return (
        <Link to={`/articulo/${articulo.id}`} className="w-1/5 bg-indigo-300 rounded p-5 shadow-lg min-h-80" key={articulo.id}>
            <img src={articulo.base64 ? `data:image/png;base64,${articulo.base64}` : articulo.imagen} alt={articulo.titulo} className='object-cover w-11/12 mx-auto pb-4 h-1/2' />
            <h3 className="font-semibold text-lg line-clamp-2">{articulo.titulo}</h3>
            <p className="px-3 pt-3 mb-3 line-clamp-3">{articulo.descripcion}</p>
        </Link>
    )
}
