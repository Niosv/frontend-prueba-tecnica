
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

export default function Navbar() {
    const selector = useSelector(state => state.periodistas.isLogged)

    const dispatch = useDispatch()

    const closeSession = (e) => {
        e.preventDefault()
        sessionStorage.removeItem("user")
        sessionStorage.removeItem("token")
        dispatch({ type: "SET_IS_LOGGED", payload: false })
    }

    return (
        <nav className='bg-blue-400 h-20 flex justify-between px-20 my-auto items-center shadow-lg'>
            <Link className='text-3xl font-semibold' to={"/"}>Prueba técnica</Link>
            <ul className='flex gap-10'>
                {selector && (
                    <li>
                        <Link to={"/addArticulo"} className='text-lg text-indigo-950 hover:text-slate-800 hover:text-xl duration-150'>Agregar articulo</Link>
                    </li>
                )}
                {!selector ? (
                    <ul className='flex gap-3 ms-10'>
                        <li >
                            <Link to={"/login"} className='bg-cyan-800 text-white px-3 py-2 rounded shadow-md hover:bg-cyan-900 hover:text-slate-200 duration-200'>Iniciar sesión</Link>
                        </li>
                        <p className='text-sm mb-0'>o</p>
                        <li>
                            <Link to={"/register"} className='bg-cyan-800 text-white px-3 py-2 rounded shadow-md hover:bg-cyan-900 hover:text-slate-200 duration-200'>Registrarse</Link>
                        </li>
                    </ul>

                ) : (
                    <ul className='flex gap-3 ms-10'>
                        <li>
                            <Link to={"/"} className='bg-red-500 text-white px-3 py-2 rounded shadow-md hover:bg-red-600 hover:text-slate-200 duration-200' onClick={(e) => closeSession(e)}>Cerrar sesión</Link>
                        </li>
                    </ul>

                )}

            </ul>
        </nav>
    )
}
