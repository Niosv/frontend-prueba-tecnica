import React from 'react'

export default function Footer() {
    return (
        <div className='absolute bottom-0 flex flex-col items-center justify-center bg-blue-300 w-full h-16'>
            <p>Prueba técnica para Analista programador en MegaMedia</p>
            <p> <span className='font-semibold'>Postulante:</span> Nicolás Cáceres</p>
        </div>
    )
}
