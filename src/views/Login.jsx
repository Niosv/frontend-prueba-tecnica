import React, { useState } from 'react'
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from 'react-router-dom'

export default function Login() {
    const [credentials, setCredentials] = useState({
        correo: "",
        passwd: ""
    })
    const dispatch = useDispatch();
    const navigate = useNavigate()

    const changeInput = (e) => {
        const { name, value } = e.target
        setCredentials({ ...credentials, [name]: value })
    }

    const handleLogin = (e) => {
        e.preventDefault()
        if (credentials.correo === "" || credentials.passwd === "") {
            return alert("Completa todos los campos")
        }
        dispatch({ type: "LOGIN_USER_REQUEST", payload: credentials })
        dispatch({ type: "SET_IS_LOGGED", payload: true })
        navigate("/")
    }

    return (
        <div className="flex items-center justify-center h-screen ">
            <form className='flex flex-col w-1/3 rounded shadow-lg bg-violet-200 px-10 py-8'>
                <section className='text-center mb-7'>
                    <h2 className="text-3xl font-semibold text-fuchsia-900 mb-3">Inicia sesión</h2>
                    <p className='text-lg'>Debes acreditarte utilizando las credenciales de periodista</p>
                </section>
                <label htmlFor="correo" className='text-lg'>Correo:</label>
                <input type="email" name="correo" id="correo" placeholder='Correo' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} />

                <label htmlFor="passwd" className='text-lg'>Contraseña:</label>
                <input type="password" name="passwd" id="passwd" placeholder='Contraseña' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} />

                <button className='bg-purple-400 w-1/2 mx-auto py-2 rounded text-white hover:shadow-md hover:bg-purple-500 hover:scale-105 duration-200' onClick={(e) => handleLogin(e)}>Iniciar sesión</button>
            </form>
        </div>
    )
}
