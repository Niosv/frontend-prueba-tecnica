import { useState } from "react"
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

export default function AddArticulo() {
    const [image, setImage] = useState(null) //Imagen mostrada
    const [articulo, setArticulo] = useState({
        titulo: "",
        descripcion: "",
        imagen: null,
        url_video: ""
    })

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const handleImageChange = (e) => {
        const selectedImage = e.target.files[0];
        setArticulo({ ...articulo, imagen: selectedImage })
        const reader = new FileReader();

        reader.onload = (event) => {
            const base64String = event.target.result;
            setImage(base64String);
        };

        reader.readAsDataURL(selectedImage);
    };

    const changeInput = (e) => {
        const { name, value } = e.target
        setArticulo({ ...articulo, [name]: value })
    }

    const handleAddArticulo = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("titulo", articulo.titulo);
        formData.append("descripcion", articulo.descripcion);
        formData.append("imagen", articulo.imagen);
        formData.append("url_video", articulo.url_video);

        console.log(formData); // Verifica la estructura de FormData

        if (articulo.titulo === "" || articulo.descripcion === "" || articulo.imagen === null || articulo.url_video === "") {
            return alert("Completa todos los campos")
        }

        dispatch({ type: "ADD_ARTICULO_REQUEST", payload: formData });
        navigate("/");
    };


    return (
        <div className="flex items-center justify-center my-10 ">
            <form className='flex flex-col w-2/3 rounded shadow-lg bg-violet-200 px-10 py-8' onSubmit={handleAddArticulo} encType="multipart/form-data">

                <h2 className="text-3xl font-semibold text-fuchsia-900 mb-7 px-10">Agrega un artículo</h2>
                <label htmlFor="titulo" className='text-lg'>Titulo:</label>
                <input type="text" name="titulo" id="titulo" placeholder='Titulo' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} />

                <label htmlFor="descripcion" className='text-lg'>Descripción:</label>
                <textarea type="text" name="descripcion" id="descripcion" placeholder='Descripción del artículo' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200 line-clamp-3 resize-none' onChange={(e) => changeInput(e)} />

                <label htmlFor="imagen" className='text-lg'>Imagen:</label>
                <input type="file" name="imagen" id="imagen" accept="image/*" placeholder='imagen' className='mb-8 mt-1 text-lg px-2 ' onChange={(e) => handleImageChange(e)} />
                {image !== null && (

                    <img src={image} alt={articulo.titulo} className="w-1/4 mx-auto object-cover" />
                )}

                <label htmlFor="url_video" className='text-lg'>Video adjunto:</label>
                <input type="text" name="url_video" id="url_video" placeholder='Aquí pega la URL del video adjunto al artículo (preferentemente de Youtube)' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} />
                <button className='bg-purple-400 w-1/2 mx-auto py-2 rounded text-white hover:shadow-md hover:bg-purple-500 hover:scale-105 duration-200' type="submit">Agregar articulo</button>
            </form>
        </div>
    )
}
