import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import Card from "../components/Card"

export default function Index() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch({ type: "GET_ARTICULO_REQUEST" });
    }, [dispatch]);

    const articulos = useSelector(state => state.articulos.articulos);


    if (articulos === null || articulos === undefined) {
        return <div>Cargando...</div>;
    }
    return (
        <>
            <div className="mx-10 my-10 py-10 bg-violet-200 shadow-xl ">
                <h2 className="text-4xl font-semibold text-center text-sky-800 mb-10">Listado de artículos</h2>
                <div className="flex flex-wrap gap-10 justify-center h-min">
                    {articulos && articulos.map((articulo) => (
                        <Card articulo={articulo} key={articulo.id} />
                    ))}
                    {articulos && articulos.length === 0 && (
                        <p className="text-lg">Aún no se han agregado artículos</p>
                    )}
                </div>

            </div>
        </>
    );
}
