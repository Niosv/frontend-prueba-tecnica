import { Link, useParams, useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { useEffect, useState } from 'react'

export default function Articulo() {
    const [isAuthor, setIsAuthor] = useState()
    const { id } = useParams() //id del Articulo
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const articulo = useSelector(state => state.articulos.articulo)
    const userLogged = JSON.parse(sessionStorage.getItem("user"))
    const embedUrl = ``;

    useEffect(() => {
        dispatch({ type: "GET_ARTICULO_BY_ID_REQUEST", payload: id })
    }, [dispatch])

    useEffect(() => {
        if (userLogged !== null && articulo !== null && articulo !== undefined) {
            setIsAuthor(userLogged.id === articulo.periodista)
        }
    }, [userLogged, articulo])

    if (articulo === null || articulo === undefined) {
        return <div>Cargando...</div>;
    }

    const handleDelete = (e) => {
        e.preventDefault()
        dispatch({ type: "DEL_ARTICULO_REQUEST", payload: id })
        navigate("/")
    }

    return (
        <div className='mx-10 my-10 p-10 bg-slate-200 shadow-xl flex flex-col gap-5 rounded relative'>
            <Link to={"/"} className='bg-violet-500 py-2 px-3 rounded text-white absolute top-5 left-5'>Volver</Link>
            <h2 className='text-center text-3xl'>{articulo.titulo}</h2>
            <p className='text-lg px-10 text-justify'>{articulo.descripcion}</p>
            <span className=' bg-slate-700 h-px w-full'></span>
            <img src={articulo.base64 ? `data:image/png;base64,${articulo.base64}` : articulo.imagen} alt={articulo.titulo} className='w-3/5 mx-auto object-cover' />
            <span className=' bg-slate-700 h-px w-full'></span>
            <iframe
                className='mx-auto'
                width="560"
                height="315"
                src={articulo ? `https://www.youtube.com/embed/${articulo.url_video}` : null}
                title="YouTube Video Player"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
            ></iframe>
            {isAuthor && (
                <>
                    <span className=' bg-slate-700 h-px w-1/2 mx-auto my-5'></span>
                    <div className='flex gap-10 justify-center'>
                        <Link to={`/modArticulo/${articulo.id}`} className='px-5 py-3 bg-yellow-400 rounded text-white font-semibold hover:bg-yellow-500 hover:scale-105 duration-200'>Modificar</Link>
                        <Link onClick={(e) => handleDelete(e)} className='px-5 py-3 bg-red-500 rounded text-white font-semibold hover:bg-red-600 hover:scale-105 duration-200'>Eliminar</Link>
                    </div>
                </>
            )}

        </div>
    )
}
