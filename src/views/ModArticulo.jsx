import { useEffect, useState } from "react"
import { useDispatch, useSelector } from 'react-redux'
import { useParams, useNavigate } from 'react-router-dom'

export default function ModArticulo() {
    const [articuloUpdated, SetArticuloUpdated] = useState({
        titulo: "",
        descripcion: "",
        imagen: "",
        url_video: "",
    })
    const [imagePreview, setImagePreview] = useState(null)

    const { id } = useParams()
    const dispatch = useDispatch()
    const articulo = useSelector(state => state.articulos.articulo);
    const navigate = useNavigate()

    useEffect(() => {
        dispatch({ type: "GET_ARTICULO_BY_ID_REQUEST", payload: id })
    }, [dispatch])

    useEffect(() => {
        if (articulo !== null) {
            SetArticuloUpdated({
                titulo: articulo.titulo,
                descripcion: articulo.descripcion,
                imagen: articulo.imagen,
                url_video: articulo.url_video,
            })
            setImagePreview(`data:image/png;base64,${articulo.base64}` || articulo.imagen)
        }
    }, [articulo])

    const handleImageChange = (e) => {
        const selectedImage = e.target.files[0];
        SetArticuloUpdated({ ...articuloUpdated, imagen: selectedImage })
        const reader = new FileReader();

        reader.onload = (event) => {
            const base64String = event.target.result;
            setImagePreview(base64String);
        };

        reader.readAsDataURL(selectedImage);
    };

    const changeInput = (e) => {
        const { name, value } = e.target
        SetArticuloUpdated({ ...articuloUpdated, [name]: value })
    }

    const handleModArticulo = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("titulo", articuloUpdated.titulo);
        formData.append("descripcion", articuloUpdated.descripcion);
        formData.append("imagen", articuloUpdated.imagen);
        formData.append("url_video", articuloUpdated.url_video);

        console.log(formData);

        if (articuloUpdated.titulo === "" || articuloUpdated.descripcion === "" || articuloUpdated.imagen === null || articuloUpdated.url_video === "") {
            return alert("Completa todos los campos")
        }

        dispatch({ type: "MOD_ARTICULO_REQUEST", payload: { formData, id } });
        navigate("/");
    };

    return (
        <div className="flex items-center justify-center  my-10 ">
            <form className='flex flex-col w-2/3 rounded shadow-lg bg-violet-200 px-10 py-8' onSubmit={handleModArticulo} encType="multipart/form-data">
                <h2 className="text-3xl font-semibold text-fuchsia-900 mb-7 px-10">Modifica un artículo</h2>
                <label htmlFor="titulo" className='text-lg'>Titulo:</label>
                <input type="text" name="titulo" id="titulo" placeholder='Titulo' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} value={articuloUpdated.titulo} />

                <label htmlFor="descripcion" className='text-lg'>Descripción:</label>
                <textarea type="text" name="descripcion" id="descripcion" placeholder='Descripción del artículo' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200 line-clamp-3 resize-none' onChange={(e) => changeInput(e)} value={articuloUpdated.descripcion} />

                <label htmlFor="imagen" className='text-lg'>Imagen:</label>
                <input type="file" name="imagen" id="imagen" accept="image/*" placeholder='imagen' className='mb-8 mt-1 text-lg px-2 ' onChange={(e) => handleImageChange(e)} />
                {articuloUpdated.imagen !== null && articulo && (

                    <img src={`${imagePreview}`} alt="Imagen del articulo" className="w-1/4 mx-auto object-cover" />
                )}

                <label htmlFor="url_video" className='text-lg'>Video adjunto:</label>
                <input type="text" name="url_video" id="url_video" placeholder='Aquí pega la URL del video adjunto al artículo' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} value={articuloUpdated.url_video} />
                <button className='bg-purple-400 w-1/2 mx-auto py-2 rounded text-white hover:shadow-md hover:bg-purple-500 hover:scale-105 duration-200' type="submit">Modificar artículo</button>
            </form>
        </div>
    )
}
