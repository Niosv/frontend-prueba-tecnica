import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'

export default function Register() {
    const [credentials, setCredentials] = useState({
        nombre: "",
        apellido: "",
        correo: "",
        passwd: ""
    })

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const changeInput = (e) => {
        const { name, value } = e.target
        setCredentials({ ...credentials, [name]: value })
    }

    const handleRegister = (e) => {
        e.preventDefault()
        if (credentials.nombre === "" || credentials.apellido === "" || credentials.correo === "" || credentials.passwd === "") {
            return alert("Completa todos los campos")
        }
        dispatch({ type: "REGISTER_USER_REQUEST", payload: credentials })
        dispatch({ type: "SET_IS_LOGGED", payload: true })
        navigate("/") //Navegar al inicio
    }

    return (
        <div className="flex items-center justify-center h-screen ">
            <form className='flex flex-col w-1/3 rounded shadow-lg bg-violet-200 px-10 py-8'>
                <section className=' text-center mb-7'>
                    <h2 className='text-3xl font-semibold text-fuchsia-900 mb-4'>¿No tienes una cuenta de periodista?</h2>
                    <p className='text-lg '>Crea una rellenando el siguiente formulario</p>
                </section>
                <div className='flex gap-5 justify-center mx-auto'>
                    <div className='w-3/6 '>
                        <label htmlFor="nombre" className='text-lg'>Nombre:</label>
                        <input type="text" name="nombre" id="nombre" placeholder='Nombre' className='mb-8 mt-1 text-lg px-2 py-1 w-full focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} />
                    </div>
                    <div className='w-3/6'>
                        <label htmlFor="apellido" className='text-lg'>Apellido:</label>
                        <input type="text" name="apellido" id="apellido" placeholder='Apellido' className='mb-8 mt-1 text-lg px-2 py-1 w-full focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} />
                    </div>
                </div>


                <label htmlFor="correo" className='text-lg'>Correo:</label>
                <input type="email" name="correo" id="correo" placeholder='Correo' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} />

                <label htmlFor="passwd" className='text-lg'>Contraseña:</label>
                <input type="password" name="passwd" id="passwd" placeholder='Contraseña' className='mb-8 mt-1 text-lg px-2 py-1 focus:outline-none focus:border-b-4 focus:border-b-violet-400 duration-200' onChange={(e) => changeInput(e)} />
                <button className='bg-purple-400 w-1/2 mx-auto py-2 rounded text-white hover:shadow-md hover:bg-purple-500 hover:scale-105 duration-200' onClick={(e) => handleRegister(e)}>Registrarse</button>
            </form>
        </div>
    )
}

