import { all } from "redux-saga/effects";
import articuloSaga from "../features/articulos/sagas";
import periodistaSaga from "../features/periodistas/sagas";

function* rootSaga() {
  yield all([
    articuloSaga(),
    periodistaSaga(),
    // Agrega otras sagas aquí si es necesario
  ]);
}

export default rootSaga;
