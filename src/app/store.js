import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./rootReducer";
import rootSaga from "./rootSaga";

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  //   reducer: {
  //     articulos: articuloSlice,
  //   },
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        // Ignorar estos tipos de acciones
        ignoredActions: ["ADD_ARTICULO_REQUEST"],
        // Ignorar estos campos en todas las acciones
        ignoredActionPaths: ["payload"],
      },
    }).concat(sagaMiddleware),
});

sagaMiddleware.run(rootSaga);
