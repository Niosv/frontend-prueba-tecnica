import { combineReducers } from "@reduxjs/toolkit";
import articuloReducer from "../features/articulos/reducers";
import periodistaReducer from "../features/periodistas/reducers";

const rootReducer = combineReducers({
  articulos: articuloReducer,
  periodistas: periodistaReducer,
});

export default rootReducer;
