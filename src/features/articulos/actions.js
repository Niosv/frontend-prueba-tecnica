import { createAction } from "@reduxjs/toolkit";

//Obtener todos los articulos
export const getArticulosRequest = createAction("GET_ARTICULO_REQUEST");
export const getArticulosSuccess = createAction("GET_ARTICULO_SUCCESS");
export const getArticulosFailure = createAction("GET_ARTICULO_FAILURE");

//Obtener un articulo por id
export const getArticuloByIdRequest = createAction(
  "GET_ARTICULO_BY_ID_REQUEST"
);
export const getArticuloByIdSuccess = createAction(
  "GET_ARTICULO_BY_ID_SUCCESS"
);
export const getArticuloByIdFailure = createAction(
  "GET_ARTICULO_BY_ID_FAILURE"
);

//Agregar un articulo
export const addArticuloRequest = createAction("ADD_ARTICULO_REQUEST");
export const addArticuloSuccess = createAction("ADD_ARTICULO_SUCCESS");
export const addArticuloFailure = createAction("ADD_ARTICULO_FAILURE");

//Modificar un articulo
export const modArticuloRequest = createAction("MOD_ARTICULO_REQUEST");
export const modArticuloSuccess = createAction("MOD_ARTICULO_SUCCESS");
export const modArticuloFailure = createAction("MOD_ARTICULO_FAILURE");

//Elimianr un articulo
export const delArticuloRequest = createAction("DEL_ARTICULO_REQUEST");
export const delArticuloSuccess = createAction("DEL_ARTICULO_SUCCESS");
export const delArticuloFailure = createAction("DEL_ARTICULO_FAILURE");
