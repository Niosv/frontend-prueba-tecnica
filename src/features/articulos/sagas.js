import { takeLatest, put, call } from "redux-saga/effects";
import {
  getArticulosRequest,
  getArticulosSuccess,
  getArticulosFailure,
  getArticuloByIdSuccess,
  getArticuloByIdFailure,
  getArticuloByIdRequest,
  addArticuloFailure,
  addArticuloRequest,
  addArticuloSuccess,
  modArticuloRequest,
  modArticuloSuccess,
  modArticuloFailure,
  delArticuloRequest,
  delArticuloSuccess,
  delArticuloFailure,
} from "./actions";

const API = "http://localhost:3000/articulo";
const token = sessionStorage.getItem("token");

function* getArticulosSaga(action) {
  try {
    const response = yield call(fetch, API);
    const articulos = yield call([response, "json"]);
    yield put(getArticulosSuccess(articulos));
  } catch (error) {
    yield put(getArticulosFailure(error));
  }
}

function* getArticuloByIdSaga(action) {
  try {
    const id = action.payload;
    const response = yield call(fetch, `${API}/${id}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const articulo = yield call([response, "json"]);
    yield put(getArticuloByIdSuccess(articulo));
  } catch (error) {
    yield put(getArticuloByIdFailure(error));
  }
}

function* addArticuloSaga(action) {
  try {
    const formData = action.payload;
    const response = yield call(fetch, API, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    });
    const data = yield call([response, "json"]);
    yield put(addArticuloSuccess(data));
  } catch (error) {
    yield put(addArticuloFailure(error));
  }
}

function* modArticuloSaga(action) {
  try {
    const { formData, id } = action.payload;
    const response = yield call(fetch, `${API}/${id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      body: formData,
    });
    const result = yield call([response, "json"]);
    yield put(modArticuloSuccess(result));
  } catch (error) {
    yield put(modArticuloFailure(error));
  }
}

function* delArticuloSaga(action) {
  try {
    const id = action.payload;
    const response = yield call(fetch, `${API}/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
    });
    const data = yield call([response, "json"]);
    yield put(delArticuloSuccess(data));
  } catch (error) {
    yield put(delArticuloFailure(error));
  }
}

function* articulosSaga() {
  yield takeLatest(getArticulosRequest.type, getArticulosSaga);
  yield takeLatest(getArticuloByIdRequest.type, getArticuloByIdSaga);
  yield takeLatest(addArticuloRequest.type, addArticuloSaga);
  yield takeLatest(modArticuloRequest.type, modArticuloSaga);
  yield takeLatest(delArticuloRequest.type, delArticuloSaga);
}

export default articulosSaga;
