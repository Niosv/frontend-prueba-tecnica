import { createReducer } from "@reduxjs/toolkit";
import {
  getArticulosSuccess,
  getArticuloByIdSuccess,
  addArticuloSuccess,
  modArticuloSuccess,
  delArticuloSuccess,
} from "./actions";

const initialState = {
  articulos: null,
  articulo: null,
  loading: false,
  error: null,
};

const articuloReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(getArticulosSuccess, (state, action) => {
      state.articulos = action.payload;
      state.loading = false;
      state.error = null;
    })
    .addCase(getArticuloByIdSuccess, (state, action) => {
      state.articulo = action.payload;
      state.loading = false;
      state.error = null;
    })
    .addCase(addArticuloSuccess, (state, action) => {
      state.articulo = action.payload;
      state.loading = false;
      state.error = null;
    })
    .addCase(modArticuloSuccess, (state, action) => {
      state.formData = action.payload;
      state.loading = false;
      state.error = null;
    })
    .addCase(delArticuloSuccess, (state, action) => {
      state.deleted = action.payload;
      state.loading = false;
      state.error = null;
    });
  //Otros reducers
});

export default articuloReducer;
