import { createReducer } from "@reduxjs/toolkit";
import { loginUserSuccess, registerUserSuccess, setIsLogged } from "./actions";

const logged = sessionStorage.getItem("user") === undefined ? false : true;

const initialState = {
  credentials: null,
  isLogged: logged,
  loading: false,
  error: null,
};

const periodistasReducer = createReducer(initialState, (builder) => {
  builder.addCase(loginUserSuccess, (state, action) => {
    state.credentials = action.payload;
    state.loading = false;
    state.error = null;
  });
  builder.addCase(registerUserSuccess, (state, action) => {
    state.credentials = action.payload;
    state.loading = false;
    state.error = null;
  });
  builder.addCase(setIsLogged, (state, action) => {
    state.isLogged = action.payload;
  });
});

export default periodistasReducer;
