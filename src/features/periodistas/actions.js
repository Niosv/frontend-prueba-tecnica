import { createAction } from "@reduxjs/toolkit";

//Login de periodista
export const loginUserRequest = createAction("LOGIN_USER_REQUEST");
export const loginUserSuccess = createAction("LOGIN_USER_SUCCESS");
export const loginUserFailure = createAction("LOGIN_USER_FAILURE");

//Registro de periodista
export const registerUserRequest = createAction("REGISTER_USER_REQUEST");
export const registerUserSuccess = createAction("REGISTER_USER_SUCCESS");
export const registerUserFailure = createAction("REGISTER_USER_FAILURE");

//¿Periodista está logeado?
export const setIsLogged = createAction("SET_IS_LOGGED");
