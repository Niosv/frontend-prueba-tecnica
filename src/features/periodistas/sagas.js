import { takeLatest, put, call } from "redux-saga/effects";
import {
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess,
  registerUserRequest,
  registerUserFailure,
  registerUserSuccess,
} from "./actions";

const API = "http://localhost:3000";

function* loginUserSaga(action) {
  try {
    const credentials = action.payload;
    const response = yield call(fetch, `${API}/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(credentials),
    });
    const data = yield call([response, "json"]);
    yield put(loginUserSuccess(data));
    //Almacenar token y user en SessionStorage
    sessionStorage.setItem("token", data.token);
    sessionStorage.setItem("user", JSON.stringify(data.user));
  } catch (error) {
    yield put(loginUserFailure(error));
  }
}

function* registerUserSaga(action) {
  try {
    const credentials = action.payload;
    const response = yield call(fetch, `${API}/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(credentials),
    });
    const data = yield call([response, "json"]);
    yield put(registerUserSuccess(data));
    //Almacenar en el sessionStorage
    sessionStorage.setItem("token", data.token);
    sessionStorage.setItem("user", JSON.stringify(data.user));
  } catch (error) {
    yield put(registerUserFailure(error));
  }
}

function* periodistasSaga() {
  yield takeLatest(loginUserRequest.type, loginUserSaga);
  yield takeLatest(registerUserRequest.type, registerUserSaga);
}

export default periodistasSaga;
